//The main() function is the entry point of dart applications

void main() {
  print(getCompanyName());
  print(getYearEctablishment());
  print(hasOnlineClasses());
  // print(getCoordinates());
  // print(combinedAddress('134 Timog Ave', 'Brgy Sacred Heart', 'QC', 'MM'));
  //partner ng normal way
  // print(combineName('Mark', 'Cobarrubias'));
  print(combineName('Mark', 'Cobarrubias'));
  print(combineName('Mark', 'Cobarrubias', isLastNameFirst: true));
  print(combineName('Mark', 'Cobarrubias', isLastNameFirst: false));

  List<String> persons = ['John Doe', 'Jane Doe'];
  List<String> students = ['Nicholas Rush', 'James Holden'];

//Anonymous function use
  // persons.forEach((String person) {
  //   print(person);
  // });

  // students.forEach((String person) {
  //   print(person);
  // });

  //Functions as objects AND used an argument
  //Print name with (value) is function execution/call/invocation
  persons.forEach(printName);
  students.forEach(printName);
}

void printName(String name) {
  print(name);
}

//normal way

//optional named parameters
// String combineName(String firstName, String lastName) {
//   return '$firstName $lastName';
// }
//These are parameters added after the required ones.
//These parameters are added inside a curly bracket.
//if no value is given, a default value can be assigned {bool? isLastNameFirst = false}
String combineName(String firstName, String lastName,
    {bool isLastNameFirst = false}) {
  if (isLastNameFirst) {
    return '$lastName, $firstName';
  } else {
    return '$firstName $lastName';
  }
}

String combinedAddress(
    String specifics, String barangay, String city, String province) {
  //  return specifics + ' ' + barangay + ', ' + city + ', ' + province;
  return '$specifics,$barangay,$city,$province';
}

String getCompanyName() {
  return 'FFUF';
}

int getYearEctablishment() {
  return 2021;
}

bool hasOnlineClasses() {
  return true;
}

// bool isUnderAge(int age){
//   return (age < 18) ? true : false
// }
//convert above to lambda function
//the initial isUnderAge function can be change into lambda or arrow function
//The syntax of the lambda function is:
//return-type function-name(parameters) => expression;
bool isUnderAge(age) => (age < 18) ? true : false;

Map<String, double> getCoordinates() {
  return {'latitude': 14.632145, 'longitude': 121.05454};
}
//The following syntax is followed when creating a function
/* 
return-typefunction-name(param-data-type parameterA, param-data-type parameterB){
  //code logic inside a function
  return 'return value
} */


//Arguments are the values being passed to the function
//parameters are the values being received by the function

//in dart writing the datatype in parameter is not required.abstract
// The reasom is relatred to code readability and conciseness