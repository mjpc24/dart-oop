void main() {
  // String firstName = 'John';

  Function discountBy25 = getDiscount(25);
  //The discountBy25 is then considered as a closure
  //The closure has access to variables in its lexical scope
  print(discountBy25(100));
}

Function getDiscount(num percentage) {
  //when the getDiscount is used and the function below is returned
  //The value of percentage parameter is retained
  //that is called lexical scope
  return (num amount) {
    return amount * percentage / 100;
  };
}
//methods are functions inside a class