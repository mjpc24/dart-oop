
void main() {
  List<num> prices = [45, 34.2, 176.9, 32.2];

  // print(getTotal(prices));
  // print(getTotal(prices, discount: 20).toStringAsFixed(2));
  // print(getTotal(prices, discount: 40).toStringAsFixed(2));
  // print(getTotal(prices, discount: 60).toStringAsFixed(2));
  // print(getTotal(prices, discount: 80).toStringAsFixed(2));
  // print(getTotal(prices, discount: 0).toStringAsFixed(2));
  print(getArraySum(prices));
  print(getDiscount(getArraySum(prices))(20));
  print(getDiscount(getArraySum(prices))(40));
  print(getDiscount(getArraySum(prices))(60));
  print(getDiscount(getArraySum(prices))(80));
  print(getDiscount(getArraySum(prices))(0));
  print(getDiscount(25)(getArraySum(prices)));
 
}

// num getTotal(List<num> prices, {num discount = 0}) {
//   if (discount == 0) {
//     num total = 0;
//     for (int i = 0; i < prices.length; i++) {
//       total = total + prices[i];
//     }
//     return total;
//   } else {
//     num total = 0;
//     for (int i = 0; i < prices.length; i++) {
//       total = total + prices[i];
//     }
//     return total - ((total * discount) / 100);
//   }
// }

//Get sum of list array
num getArraySum(prices) {
  num sum = 0;
  prices.forEach((num p) {
    sum = sum += p;
  });
  return sum;
}

Function getDiscount(num percentage) {
  return (num amount) {
    return amount * percentage / 100;
  };
}
//best Practive
//A variable must be a noun
//A function must be verb


/* 
//solution ni sir

void main(){
  List<num> prices = [45, 34.2, 176.9, 32.2];
  num totalPrice = 0;

  prices.forEach((num price){
    totalPrice = price;
  })
  print(totalPrice);



}

 */