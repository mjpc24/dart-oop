void main() {
  List<num> prices = [45, 34.2, 176.9, 32.2];

  print(getTotal(prices));
  print(getTotal(prices, discount: 20).toStringAsFixed(2));
  print(getTotal(prices, discount: 40).toStringAsFixed(2));
  print(getTotal(prices, discount: 60).toStringAsFixed(2));
  print(getTotal(prices, discount: 80).toStringAsFixed(2));
  print(getTotal(prices, discount: 0).toStringAsFixed(2));
}

num getTotal(List<num> prices, {num discount = 0}) {
  if (discount == 0) {
    num total = 0;
    for (int i = 0; i < prices.length; i++) {
      total = total + prices[i];
    }
    return total;
  } else {
    num total = 0;
    for (int i = 0; i < prices.length; i++) {
      total = total + prices[i];
    }
    return total - ((total * discount) / 100);
  }
}
