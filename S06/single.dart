void main() {
  Person person = new Person(firstName: 'John', lastName: 'Smith');
  Employee employee = new Employee(firstName: 'Mj', lastName: 'Coba');

  print(person.getFullName());
  print(employee.getFullName());
}

class Person {
  String firstName;
  String lastName;

  Person({required this.firstName, required this.lastName});

  String getFullName() {
    return '${firstName} ${this.lastName}';
  }
}

class Employee extends Person {
//Employee inherits Person
//the employee also inherits the getFullName from person
//Inheritance then allows us to write less codes
  

  Employee({required String firstName, required String lastName})
      : super(
        firstName: firstName, 
        lastName: lastName);
}
