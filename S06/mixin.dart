void main() {

  Equipment equipment = new Equipment();
  equipment.name = 'Equipment-001';
  print(equipment.name);

  Loader loader = new Loader();
  loader.name = 'Loader-001';
  print(loader.name);
  print(loader.getCategory());
  
  loader.moveForward(10);
  loader.moveBackward();
  // loader.makeFamily();
  Car car = new Car();
  car.name = 'Car-001';
  print(car.name);
  print(car.getCategory());
  car.moveForward(10);
  car.moveBackward();
}
class Equipment {
  String? name;
}
class Loader extends Equipment with Movement {
  String getCategory() {
    return '${this.name} is a loader.';
  }
}
class Car with Movement {
  String? name;
  String getCategory() {
    
    return '${this.name} is a car';
  }
}

mixin Movement {
  num? acceleration;

  void moveForward(num acceleration){
    this.acceleration = acceleration;
    print('The vehicle is moving forward');
  }
  void moveBackward(){
    print('The vehicle is moving Backward');
  }

}
// Test kung pwede
// mixin Multiply {
 
//   void makeFamily(){
//     print('They multiply');
//   }

// }