//To know when to use late vs required
//use 'required when logically, you always require that varioable to have a value
//Then use required for both fisrtName and lastName fields
//do i always need

//Use 'late' when logically a variable can have a value
//a person ,ay or ,ay not have a spouse
//the use late for spouseId field
//use late if you are sure that later on your program , the value will be given to that variable
//else ,use the null-enabler instead (?)

class Building {

  //incase taht there is a  nullable value put ? and remove required
  //_ stands for private
  String? _name;
  int floors;
  String address;

//constructor
  Building(
    this._name, {
    required this.floors,
    required this.address,
  }) {
    print('A new Building was created');
  }
//no need for () unlike n JS
  String? get Name {
    print('Building has been retreived');
    return this._name;
  }
//with void since no expected return
  void set Name(String? name) {
    this._name = name;
    print('The building\'s name has been changed');
  }

  Map<String, dynamic> getProperties() {
    return {'name': this._name, 'floors': this.floors, 'address': this.address};
  }
}
