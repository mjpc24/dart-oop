void main() {
  //adding instance
  //one way
  // Building building = new Building();
  // building.name = 'Caswynn';
  // building.floors = 8;
  // building.address = '0172 saudi village Lunsad Bin Rizal';
  //or this one but you have to provide the constructor below then remoe ?
  Building building = new Building(
      name: 'Caswynn',
      floors: 8,
      address: '0172 saudi village Lunsad Bin Rizal',
      rooms: ['Tech', 'Dev']);

  print(building);
  print(building.name);
  print(building.floors);
  print(building.address);
  print(building.rooms);
}

class Building {
  //incase that there is a  nullable value put ? and remove required
  //declaration of private
  String? name;
  int floors;
  String address;
  List<String> rooms;
//constructor
  Building(
      {this.name,
      required this.floors,
      required this.address,
      required this.rooms}) {
    print('A new Building was crated');
  }
}

// After adding
// class Building {
//   String name;
//   int floors;
//   String address;
//   List<String> rooms;

//   Building(this.name, this.floors, this.address, this.rooms) {
//     print('A new Building was crated');
//   }
// }


// class Building {
//    late String name;
//    late int floors;
//    late String address;

//   Building(String name, int floors, String address) {
//     this.name = name;
//     this.floors = floors;
//     this.address = address;
//   }
// }




//the class declaration looks like this:

/* 
  class ClassName {
    <field>
    <constructors>
    <methods>
    <getters-or-setters>
  }


 */
