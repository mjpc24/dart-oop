// Create an s08/main.dart file and put the comments here to the Dart file.

// Create an abstract class named Equipment and an 
// abstract method named describe inside the abstract class.

// Create the classes Bulldozer, TowerCrane, and Loader 
// that will all implement the abstract class Equipment.

// Create objects for each of the vehicle type with the following information.
// - Bulldozer: Caterpillar D10, U blade
// - Tower Crane: 370 EC-B 12 Fibre, 78m hook radius, 12,000kg max capacity
// - Loader: Volvo L60H, wheel loader, 16530lbs tipping load

// Inside the main() method, create a List for the 
// equipment (using the Equipment type) and give it an initial value of [].

// Add the created objects to the list created earlier.

// Loop through each vehicle in the list and execute the describe() method.

// Sample Output
// - The bulldozer Caterpillar D10 has a U blade.
// - The tower crane 370 EC-B 12 Fibre has a radius of 78 and a max capacity of 12000.
// - The loader Volvo L60H is a wheel loader and has a tipping load of 16530 lbs.

void main(){

  List<dynamic> equipment = [];

  //Create bulldozer
  Bulldozer bulldozer1 = new Bulldozer(model: 'Caterpillar', equipmentType: 'U Blade');
  //Create towercrane
  TowerCrane towercrane1 = new TowerCrane(model: '370 EC-B 12 Fibre', hookRadius: '78m hook radius', maxCapacity: 12000);
  //Create Loader
  Loader loader1 = new Loader(model: 'Volvo L60H', type: 'wheelLoader', tippingLoad: 16530);


  equipment.add(bulldozer1);
  equipment.add(towercrane1);
  equipment.add(loader1);
  // print(equipment);
  equipment.forEach((element) {
    print(element.describe());
  });
}

abstract class Equipment {
  String describe();
}


class Bulldozer implements Equipment {
  String model;
  String equipmentType;

  Bulldozer({
    required this.model,
    required this.equipmentType
  }){
    print('New Bulldozer added');
  }

  String describe(){
    return 'The Bulldozer ${this.model}  has a ${this.equipmentType}';
  }
}

class TowerCrane implements Equipment {
  String model;
  String hookRadius;
  num maxCapacity;

  TowerCrane({
    required this.model,
    required this.hookRadius,
    required this.maxCapacity
  }){
    print('New TowerCrane added');
  }
  String describe(){
    return 'The tower crane ${this.model} has a radius of ${this.hookRadius} and a max capacity of ${this.maxCapacity.toString()}';
  }
}

class Loader implements Equipment  {
  String model;
  String type;
  num tippingLoad;

  Loader({
    required this.model,
    required this.type,
    required this.tippingLoad
  }){
    print('New Loader added');
  }

  String describe(){
    return 'The loader ${this.model} is a ${this.type} and has a tipping load of ${this.tippingLoad.toString()} lbs';
  }
}