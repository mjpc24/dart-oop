void main() {
  //assignment operator
  int x = 1397;
  int y = 7831;

  //Arithmetic operators
  num sum = x + y;
  num difference = x - y;
  num product = x * y;
  num quotient = x / y;
  num remainder = 4 % 3;
  num output = (x * y) - (x / y + x);

//Relational
  bool isGreaterThan = x > y;
  bool isLessThan = x < y;

  bool isGTorEqual = x >= y;
  bool isLTorEqual = x >= y;
  bool isEqual = x == y;
  bool isNotEqual = x != y;

//Logical operators
  bool isLegalAge = true;
  bool isRegistered = false;

  bool areAllRequirementsMet = isLegalAge && isRegistered;
  bool areSomerequirementsMet = isLegalAge || isRegistered;
  bool IsNotRegistered = !isRegistered;

//Increment & decrement print(x++);
  print(y--);
  print(x++);
}
