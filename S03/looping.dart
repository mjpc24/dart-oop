void main() {
  // whileLoop();
  // doWhileLoop();
  // forLoop();
  forInLoop();
  modifiedForLoop();
}

void whileLoop() {
  int count = 5;

  while (count != 0) {
    print(count);
    count--;
  }
}

void doWhileLoop() {
  int count = 20;

  do {
    print(count);
    count--;
  } while (count > 0);
}

void forLoop() {
  //syntax: initializer,condition,iterator
  for (int count = 0; count <= 20; count++) {
    print(count);
  }
}

void forInLoop() {
  //List<Map<String, String>>
  List persons = [
    {'name': 'Brandon'},
    {'name': 'John'},
    {'name': 'Arthur'}
  ];

  for (var person in persons) {
    print(person);
    print(person['name']);
  }
}

void modifiedForLoop() {
  for (int count = 0; count <= 20; count++) {
    if (count % 2 == 0) {
      continue;
    } else {
      print(count);
    }
    if (count > 10) {
      break;
    }
  }
}
