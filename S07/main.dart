
import './worker.dart';

void  main(){
    Doctor doctor = new Doctor(firstName: 'John',lastName: 'Smith');
    Carpenter carpenter = new Carpenter();

  // Person personA = new Person();

  print(carpenter.getType());
}


abstract class Person {
  //The class PErson defines that a 'getFullName' must be implemented
  //However, it does not tell the classs HOW to implement it

  String getFullname();
}

//concrete class is the doctor class
//implements
class Doctor implements Person {
  String firstName;
  String lastName;

  Doctor({
    required this.firstName,
    required this.lastName
  });

  String getFullname(){
    return 'Dr. ${this.firstName} ${this.lastName}';
  }
}

// class Attorney implements Person {
//   String getFullName(){
//       return 'Atty ';
//   }
// }