//importing this is 'always' needed
import 'package:flutter/material.dart';
import './body_questions.dart';
import './body_answers.dart';
void main(){
  //App is the root widget
    runApp(new App());
}
//statelesswidget is used when nothing will be changed
class App extends StatefulWidget {
    @override 
    AppState createState() => AppState();
}
class AppState extends State<App>{
    int questionIdx = 0;
    bool showAnswers = false;
  // int answersIdx = 0;

  // List<String> questions = [
  //   'What is the nature of your business needs?',
  //   'What is the expected size of the user base?',
  //   'In which region would the majority of the user base be?',
  //   'What is the expected project duration?'
  // ];

  // List<List<String>> answers = [
  //   ['Time Tracking','Asset Management','Issue Tracking'],
  //   ['Less than 1,000','Less than 10,000','More than 1,000'],
  //   ['Asia','Europe','Americas','Africa','Middle East'],
  //   ['Less than 3 months','3-6 months','6-9 months','9-12 months','More tha 12 months']

  // ];

    final questions = [
        {
        'question': 'What is the nature of your business needs?',
        'option': ['Time Tracking',
                    'Asset Management',
                    'Issue Tracking'
                    ]
        },
        {
        'question': 'What is the expected size of the user base?',
        'option': ['Less than 1,000',
                    'Less than 10,000',
                    'More than 1,000'
                    ]
        },
        {
        'question': 'In which region would the majority of the user base be?',
        'option': ['Asia',
                'Europe',
                'Americas',
                'Africa',
                'Middle East'
                ]
        },
        {
        'question': 'What is the expected project duration?',
        'option': ['Less than 3 months',
                    '3-6 months',
                    '6-9 months',
                    '9-12 months',
                    'More tha 12 months'
                    ]

        }

    ];
    
    var answers  = [];
    
    void nextQuestion(String? answer){
        answers.add({
            'question': questions[questionIdx]['question'],
            'answer' : (answer == null) ? '' : answer
        });
        print(answers);
        if (questionIdx < questions.length -1){
            setState(() =>questionIdx++);
        } else {
            setState(() =>showAnswers = true);
        }
    }
    //implemented build method by clicking the error and click quick fix and choose override
    @override
    Widget build(BuildContext context) {
        //Two(2) types of widgets 
        //invisible
        //visible
        //The invisile widget are container and scaffold
        //The visible widgets are AppBar and Text
        //in flutter,width = double.infinity is the equivalent of width =100% in css
        //To put colors on a container, the decoration: BoxDecoration(Colors.green) can be used
        //in a Column widget, the main axis alignment is vertical( or top to bottom)

        //To change the horizontoal alignment of widgets in a column, we must use the crossAxixAligment
        //for example, if you want to place column in widgets to the horizontal left, we use CrossAxisAlignment.start
        //In the context of the column, we can consider the column as vertical, and its cross axis alignment
        
                    //responsible for building basic design and layout structure
                    //The scaffold can be given UI elements such as an app bar
                var bodyQuestions = BodyQuestions(
                    questions: questions,
                    questionIdx: questionIdx,
                    nextQuestion: nextQuestion
                    );
                var bodyAnswers = BodyAnswers(answers: answers);

                Scaffold homepage =Scaffold(
                    appBar: AppBar(
                    title:Text('Homepage')),
                    body: (showAnswers) ? bodyAnswers : bodyQuestions
                );
                    return MaterialApp(
                        home: homepage //home is the index.html file counterpart
                    ); 

        /* MaterialApp
            Scaffold
            Appbar
            Text
                Container
                    Text */
    }

}
