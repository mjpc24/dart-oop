import 'package:flutter/material.dart';
import './answer_button.dart';

class BodyQuestions extends StatelessWidget{
    final  questions;
    final int questionIdx;
    final Function nextQuestion;

    BodyQuestions({
        required this.questions,
        required this.questionIdx,
        required this.nextQuestion
    });
    @override
    Widget build(BuildContext context) {
        Text questionText =  Text(
                questions[questionIdx]['question'].toString(),
                style: TextStyle(
                fontSize :24.0
                )
        );
        var options = questions[questionIdx]['option']as List<String>;
        var answewrOptions = options.map((String option){
            return AnswerButton(text: option, nextQuestion: nextQuestion);
        });
        return Container(
            //to specify margin or spacing you may use EdgeInsets.
        //The values for spacing are in multiples of 8 (eg. 16,24,32)
        //to add spacing an all sides use EdgeInsets.all().
        //To add spacing on certain sides, use EdgeInsets.only(direction: value)
        //EdgeInsets hover for details
            width: double.infinity,
            padding: EdgeInsets.all(16) ,
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
            // mainAxisAlignment:  MainAxisAlignment.center,
                children: [
                    questionText,
                    //three dot means spread operator 
                    //[questionText, [answerOptions],skipQuestion]
                    //from this to this
                    //[questionText,answerButtonA,answerButtonB,answerButtonC, skipQuestion]
                    ...answewrOptions,
                    Container(
                        margin: EdgeInsets.only(top:30),
                        width: double.infinity,
                        child: ElevatedButton(
                        child: Text('Skip Question'),
                        onPressed: () => nextQuestion(null)
                            )
                    )

                ]
            ) 
        );
    }   
}

//The state is lifted up
//App.questionIdx
// -> BodyQuestions.questionsIdx =1