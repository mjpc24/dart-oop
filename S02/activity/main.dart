void main() {
  List<String> processors = [
    'Ryzen 3 3200G',
    'Ryzen 5 3600',
    'Ryzen 7 5800X',
    'Ryzen 9 5950X'
  ];

  Map<String, String> person = {
    'firstName': 'John',
    'lastName': 'Smith',
    'Age': '34'
  };
//Added both list and Set for this depends on the expected output due to redundant value "Canada"
  Set<String> countries = {
    'Thailand',
    'Philippines',
    'Singapore',
    'Japan',
    'Canada',
    'UnitedKingdom',
    'Canada'
  };

 List<String> countries1 = [
    'Thailand',
    'Philippines',
    'Singapore',
    'Japan',
    'Canada',
    'UnitedKingdom',
    'Canada'
 ];

  print(countries);
  print(countries1);
  print(processors);
  print(person);
  print(countries.length);
  print(countries1.length);
  print(processors.length);
  print(person.length);
}
