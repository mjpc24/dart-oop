// Set<dataType> variableName = {values}- compare to Map() set is an object that is not a key:value pair

void main() {
  //A set in dart is an unordered collection of unique items
  Set<String> subContractors = {'Sonderhoff', 'Stahlschidmt'};

//Adding value
  subContractors.add('Schweisstechnik');
  subContractors.add('Kreatel');
  subContractors.add('Kunstoffe');

//Removing
  subContractors.remove('Sonderhoff');

  print(subContractors);
}
