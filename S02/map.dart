void main() {
  Map<String, String> address = {
    'specifics': '221B Baker Street',
    'district': 'marylebone',
    'city': 'London',
    'country': 'United Kingdom'
  };

//Updating
  address['region'] = 'Europe';

  print(address);
}
