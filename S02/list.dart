//List<dataType> variableName = [values]

void main() {
  List<int> discountRanges = [20, 40, 60, 80, 1];
  List<String> names = ['John', 'Jane', 'Tom'];

  const List<String> maritalStatus = [
    'single',
    'married',
    'divorced',
    'widowed'
  ];
  print(discountRanges);
  print(names);

  //retreiving specific value in the array
  print(discountRanges[2]);
  print(names[0]);
  //length of the array
  print(discountRanges.length);
  print(names.length);
//Update value in array
  names[0] = 'Jonathan';
  print(names);
  //adding value in array
  names.add('Mark');
  names.insert(0, 'Mark'); //adding elements in your desired position

  print(names);

  //these are properties
  print(names.isEmpty);
  print(names.isNotEmpty);
  print(names.first);
  print(names.last);
  print(names.reversed);
//These are methods
  names.sort();
  print(names);
  discountRanges.sort();
  print(discountRanges);
  print(names.reversed);
}
