import './freezed_models/user.dart';

void main() {
    User userA = new User(id: 1, email: 'john@gmail.com');
    User userB = new User(id: 1, email: 'john@gmail.com');

    // print(userA.hashCode);
    // print(userB.hashCode);
    // print(userA == userB);


    //demonstration of object immuability below
    //immutability means changes are not allowed
    //it ensures that an object will not be changed accidentally


    // print(userA.email);
    //wrong way to update
    // userA.email = 'john@hotmail.com';
    //proper
    // userA = userA.copyWith(email:'john@hotmail.com' );
    // print(userA.email);


    var response = {'id': 3, 'email': 'doe@gmail.com'};

    // User userC = User(
    //     id: response['id'] as int,
    //     email:response['email'] as String
    //      );


    //since we have json
    User userC = User.fromJson(response);
    print(userC);
    print(userC.toJson());


   
}