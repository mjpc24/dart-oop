//import freezed annotation
import 'package:freezed_annotation/freezed_annotation.dart';
//
part 'user.freezed.dart';
part 'user.g.dart';

//The freezed annotation tells the buld_runner to create a freexed class named _$User
@freezed 

class User with _$User {
    const factory User ({
        required int id,
        required String email,
        String? accessToken
    }) = _User;

    factory User.fromJson(Map<String,dynamic> json) => _$UserFromJson(json);
}