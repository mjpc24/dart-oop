//import freezed annotation
import 'package:freezed_annotation/freezed_annotation.dart';
//
part 'note.freezed.dart';
part 'note.g.dart';

//The freezed annotation tells the buld_runner to create a freexed class named _$User
@freezed 

class Note with _$Note {
    const factory Note ({
        required int id,
        required int userId,
        required String title,
        required String description
    }) = _Note;

    factory Note.fromJson(Map<String,dynamic> json) => _$NoteFromJson(json);
}