//The void means the function will not return anything

//syntax return-type <void> funtion name <main> parameters and {}
void main() {
  // In declaring variable data type must be declared.
  // var will vary to the value provided
  print('Hello World');
  String firstName = '50';
  String lastName = 'Smith';
  String? middleName =
      null; //for dart to accept the null value you must put ? after the datatype declaration
  int age = 31; //use for numbers without decimal points
  double height = 172.45; // for numbers with decimal value
  num weight = 64.32; //can accept num with or without decimal points
  bool isRegistered = false;
  List<num> grades = [98.2, 89, 87.88, 91.2]; //array equivalent
  // Map person = new Map();Map is an object, one method is to create a map
  //You may omit declaring specific data type declaration <String,dynamic>
  Map personA = {'name': 'Brandon', 'batch': 213};

  //Alternative syntax for declaring variables
  Map<String, dynamic> personB = new Map();
  personB['name'] = 'Juan';
  personB['batch'] = 89;

  //With final once value has been set, it cannot be changed
  // final DateTime now = DateTime.now();//OR CAN BE WRITTEN LIKE BELOW
  final DateTime now;
  now = DateTime.now();

  //With const, an identifier must have a corresponding declaration of value
  //cannot be reassisgned when using const
  const String companyAcronym = 'FFUF';
  // companyAcronym = 'FFUF';

  print(firstName +
      ' ' +
      lastName); //Hello is concatinated with the main variuable
  print('Full Name: $firstName $lastName');
  print('Age:' + age.toString());
  print('Height:' + height.toString());
  print('weight:' + weight.toString());
  print('Registered: $isRegistered');
  //other way direct approach to print using php style
  print('Grades:' + grades.toString());
  print(personA);
  print(personB);
  print('Current Datetime: ' + now.toString());

  //to access data inside object
  print(personB['name']);
  print(grades);
  print(companyAcronym);
}
